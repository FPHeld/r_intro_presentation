First Steps in R
===================================
author: Fabian P. Held
date: xx. February 2015
transition: linear
for CPC EMCR, the University of Sydney

Topics Covered
=====================================
type: section

- What is R?
- R Installation
- R Studio
- Data in R
- Statistical Models
- Help
- Plotting


What is R?
======================
type: sub-section
R is a free software environment for statistical computing and graphics.  
It compiles and runs on a wide variety of UNIX platforms, Windows and MacOS.

- Modular (extend capabilities by loading packages)
- Strong graphics features
- Made for flexibility, not for speed
- Available under [www.r-project.org/](www.r-project.org/)

R Installation
========================================================
type: sub-section
- Download from [www.r-project.org/](www.r-project.org/)
- Select any mirror (preferably in Australia)
- Install with defaults
    + Select Internet2
    + Call IT on 16000

First Steps
==========================================================
type: sub-section

```r
3 + 5
```

```
[1] 8
```


```r
a <- 3 
b <- 5
a + b
```

```
[1] 8
```


```r
add <- function(x, y) x + y
add(a, b)
```

```
[1] 8
```

First Steps
==========================================================
type: sub-section

```r
add_numbers <- function(x=0, y=0){
    stopifnot(is.numeric(x), 
              is.numeric(y))
    x + y
    }
add_numbers(y=a, x=b)
```

```
[1] 8
```

Use an Editor 
========================================================
type: sub-section
- Many tools are available to create and manage longer R scripts
- Recommended: RStudio [http://www.rstudio.com/](http://www.rstudio.com/)
    + Syntax Highlighting
    + Package Management
    + Tracks your Working Environment
    + History
    + Output as Pdf, HTML, Presentation (RMarkdown)
    + Version Control (Git, SVN)
    + Shiny integration

Find Help
========================================================
type: sub-section
- F1 (read the manual)
    + Copy all arguments to your syntax and delete what you don't need
    + ?
    + ??
- Google
    + [http://stackoverflow.com/](Stackoverflow)
    + [http://www.cookbook-r.com/](R Cookbook)

Data in R
========================================================
type: sub-section
Data Structures
- Data Frames 
- Vectors
- Lists
- Matrices
- Arrays

*** 
Types
- character
- double
- integer
- logical

Data Frames
========================================================
type: sub-section

```r
df <- data.frame(x = 1:3, 
                 y = c("a", "b", "c"))

print(df)
```

```
  x y
1 1 a
2 2 b
3 3 c
```

Data Frames
========================================================
type: sub-section

```r
str(df)
```

```
'data.frame':	3 obs. of  2 variables:
 $ x: int  1 2 3
 $ y: Factor w/ 3 levels "a","b","c": 1 2 3
```

```r
df$x
```

```
[1] 1 2 3
```

```r
df[["x"]]
```

```
[1] 1 2 3
```

```r
df[1,]
```

```
  x y
1 1 a
```

Data into R
========================================================
type: sub-section

```r
data <- read.csv(file=".\rawdata\some_file.csv", 
        header = TRUE, 
        sep = ",", 
        quote = "\"",
        dec = ".", 
        colClasses=NA,
        fill = TRUE, 
        stringsAsFactors=TRUE,
        )
```

Reads and writes .txt, .xls, .xlsx, .spss, .xlm, .dta, .ssd and many more
 - see libraries "xlsx", "foreign"
 
Also connects to data bases


Statistical Models
========================================================
type: sub-section

```r
my_analysis <- lm(formula=, 
                  data, 
                  subset, 
                  weights, 
                  na.action,
                  method = "qr", 
                  model = TRUE, 
                  x = FALSE, 
                  y = FALSE, 
                  qr = TRUE,
                  singular.ok = TRUE, 
                  contrasts = NULL, 
                  offset
                  )
```
 - next: assess quality, plot, etc.

Style Guide I
========================================================
type: sub-section
[Advanced R: Style](http://adv-r.had.co.nz/Style.html)
- File names should be meaningful and end in .R.
- Variable and function names should be lowercase. 
- Use an underscore (_) to separate words within a name. 
- Generally, variable names should be nouns and function names should be verbs. 
- Place spaces around all infix operators (=, +, -, <-, etc.). The same rule applies when using = in function calls. 
- Always put a space after a comma, and never before (just like in regular English).

Style Guide II
========================================================
type: sub-section
- An opening curly brace should never go on its own line and should always be followed by a new line. 
- A closing curly brace should always go on its own line, unless it’s followed by else.
- Strive to limit your code to 80 characters per line.
- Comment your code. Each line of a comment should begin with the comment symbol and a single space: #. 
- Comments should explain the why, not the what.
- Use commented lines of - and = to break up your file into easily readable chunks.

Organise Data and Projects
=======================================================
type: sub-section
Use projects and code folding in RStudio

Use folders:
- raw_data
- working_data
- output

Organise your Code
=======================================================
type: sub-section

- read_and_clean_data.R
- often_used_functions.R
- analysis.R

Begin `analysis.R` with 
 

```r
source(file="read_and_clean_data.R")
source(file="often_used_functions.R")

my_analysis <- do_things(data)
plot(my_analysis)
```

Plotting
========================================================
type: sub-section
jpg, **png**, bmp, tiff, **pdf**


```r
png(filename = "Rplot%03d.png",
    width = 480, height = 480, 
    units = "px", pointsize = 12,
    bg = "white", res = NA, family = "", 
    restoreConsole = TRUE,
    type = c("windows", "cairo", "cairo-png"), antialias)

plot(my_analysis)
dev.off()
```
Don't use jpg, ever

My Favourite Packages
=========================================================
type: sub-section
- dplyr
- reshape2
- ggplot2
- colorspace

honourable mentions
- plyr
- parallel
- lattice

Further References
=========================================================
type: sub-section
- [Advanced R](http://adv-r.had.co.nz/) 
- [Stackoverflow](http://stackoverflow.com/)
- [R Cookbook](http://www.cookbook-r.com/)
- read functions' source code
- share scripts with others 
- help each other
- [Sydney Users of R Forum](http://www.meetup.com/R-Users-Sydney/)


